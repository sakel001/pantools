# PanTools version 2
PanTools is a pangenomic toolkit for comparative analysis of large number of genomes. It is developed in the Bioinformatics Group of Wageningen University, the Netherlands. Please cite the relevant publication(s) from the list of publications if you use PanTools in your research.

## Licence
PanTools has been licensed under [GNU GENERAL PUBLIC LICENSE version 3](https://www.gnu.org/licenses/gpl-3.0.en.html).

## Publications
- [PanTools: representation, storage and exploration of pan-genomic data.](https://academic.oup.com/bioinformatics/article/32/17/i487/2450785)
- [Efficient inference of homologs in large eukaryotic pan-proteomes.](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-018-2362-4)
- [Pan-genomic read mapping]()

## Functionalities
PanTools currently provides these functionalities:

- Construction of a pangenome
- Construction of a panproteome
- Adding new genomes to the pangenome
- Adding structural annotations to the genomes
- Detecting homology groups based on similarity of proteins
- Retrieving features/regions/genomes
- Read mappping


## Requirements
- **[Java Virtual Machine](https://www.java.com/en/download/)** version 1.8 or higher,
Add path to the java executable to your OS path environment variable.
- **[KMC](http://sun.aei.polsl.pl/kmc)**: A disk-based k-mer counter,
       After downloading the appropriate version (linux, macos or windows), add path to the *kmc* and *kmc_tools* executables to your OS path environment variable.
- **[MCL](http://micans.org/mcl)**: The Markov Clustering Algorithm,
       After downloading and compiling the software, add path to the *mcl* executable to your OS path environment variable.

## Running the program 
Add the path to the java archive of PanTools, located in the /dist subdirectory of PanTools project, to the OS path environment variable. Then run PanTools from the command line by:   

	java <JVM options> -jar pantools.jar <subcommand> <arguments>


### Useful JVM options

- **-server** : To optimize JIT compilations for higher performance
- **-Xmn(a number followed by m/g)** : Minimum heap size in mega/giga bytes
- **-Xmx(a number followed by m/g)** : Maximum heap size in mega/giga bytes


### Sub-commands
- **build_pangenome** or **bpg** : To build a pangenome out of a set of genomes.

   **arguments:**
   - *--database_path* or *-dp* : Path to the pangenome database. 
   - *--genomes-file* or *-gf* : A text file containing paths to FASTA files of genomes;       each on a seperate line.
   - *--kmer-size* or *-ks* : The size of k-mers. If it is not given or is out of valid range (6 <= *K_SIZE* <= 255), then an optimal value will be calculated automatically.    


- **build_panproteome** or **bpp** : To build a panproteome out of a set of proteins.

   **arguments:**
   - *--database_path* or *-dp* : Path to the pangenome database. 
   - *--proteomes_file* or *-pf* : A text file containing paths to FASTA files of proteomes; each in a seperated line.

        
- **add_genomes** or **ag** : To add new genomes to an available pangenome.  

    **arguments:**
   - *--database_path* or *-dp* : Path to the pangenome database. 
   - *--genomes-file* or *-gf* : A text file containing paths to FASTA files of the new genomes to be added to the pangeome; each in a seperated line.


- **add_annotations** or **aa** : To add annotations to existing genomes. 

   **arguments:**
   - *--database_path* or *-dp* : Path to the pangenome database. 
   - *--output-path* or *-op* (**default value**: Database path determined by *-dp*) : Path to the output files. 
   - *--annotations-file* or *-af* : A text file each line of which contains genome number and path to the corresponding GFF file separated by one space. Genomes are numbered in the same order they have been added to the pangenome. The protein sequence of the annotated genes will be also stored in the folder "/proteins" in the output path. 
   - *--connect_annotations* or *-ca* : Connects the annotated genomic features to the nodes of gDBG.


- **retrieve_features** or **rf** : To retrieve the sequence of annotated features from the pangenome. For each genome a FASTA file containing the retrieved features will be stored in the output path. For example, genes.1.fasta contains all the genes annotated in genome 1.
   
   **arguments:**
   - *--database_path* or *-dp* : Path to the pangenome database. 
   - *--output-path* or *-op* (**default value**: Database path determined by *-dp*) : Path to the output files. 
   - *--genome-numbers* or *-gn* : A text file containing genome_numbers for which the features will be retrieved.
	- *--feature-type* or *-ft* (**default value**: gene) : The feature name; for example gene, mRNA, exon, tRNA, etc.


- **retrieve_regions** or *rr* : To retrieve the sequence of some genomic regions from the pangenome. The resulting FASTA files will be stored in the output path.

   **arguments:**
   - *--database_path* or *-dp* : Path to the pangenome database. 
   - *--output-path* or *-op* (**default value**: Database path determined by *-dp*) : Path to the output files. 
   - *--regions-file* or *-rf* : A text file containing records with genome_number, sequence_number, begin and end positions separated by one space for each region.


- **retrieve_genomes** or **rg** : To retrieve the full sequence of some genomes. The resulting FASTA files will be stored in the output path.

   **arguments:**
   - *--database_path* or *-dp* : Path to the pangenome database. 
   - *--output-path* or *-op* (**default value**: Database path determined by *-dp*) : Path to the output files. 
   - *--genome-numbers* or *-gn* : A text file containing genome_numbers to be retrieved in each line..


- **group** or **g** : To create homology groups in the protein space of the pangenome (panproteome). The resulting homology groups will be stored in the output path. 

   **arguments**:
   - *--database_path* or *-dp* : Path to the pangenome database. 
   - *--output-path* or *-op* (**default value**: Database path determined by *-dp*) : Path to the output files. 
   - *--intersection-rate* or *-ir* (**default valuue**: 0.09, **valid range**: [0.001..0.1]) : The fraction of k-mers needs to be shared by two intersecting proteins.
   - *--min-normalized-similarity* or *-mns* (**default value**: 95, **valid range**: [1..99]) : The minimum normalized similarity score of two proteins.
   - *--mcl-inflation* or *-mi* (**default value**: 9.6, **valid range**: (1..19)): The MCL inflation.
   - *--contrast* or *-ct* (**default value**: 8, **valid range**: (0..10)) : The contrast factor.
   - *--relaxation* or *rn* (**default value**: 1, **valid range**: [1..8]) : The relaxation in homology calls.
   - *--threads-number* or *-tn* (**default value**: 1) : The number of parallel working threads.


- **map** or **m** : To map single or paired-end short reads to all or a sebset of the constituent genomes. The resulting SAM/BAM files will be stored in the output path.

   **argument**:
   - *--database_path* or *-dp* : Path to the pangenome database. 
   - *-1* : The first short-read archive in FASTQ format, which can be gz/bz2 compressed. This file can be precessed interleaved by -il option.
   - *-2* : Optionally, the second short-read archive in FASTQ format, which can be gz/bz2 compressed. 
   - *--genome-numbers* or *-gn* : A text file containing genome_numbers to map reads against in each line. 
   - *--output-path* or *-op* (**default value**: Database path determined by *-dp*) : Path to the output files. 
   - *--threads-number* or *-tn* (**default value**: 1) : The number of parallel working threads.
   - *--min-mapping-identity* or *-mmi* (**default value**: 0.5, **valid range**: [0..1)) : The minimum acceptable identity of the alignment.
   - *--num-kmer-samples* or *-nks (**default value**: 15, **valid range**: [1..r-k+1]) : The number of kmers sampled from read. 
   - *--min-hit-length* or *-mhl* (**default value**: 13, **valid range**: [10..100]) : The minimum acceptable length of alignment after soft-clipping.
   - *--max-alignment-length* or *-mal* (**default value**: 1000, **valid range**: [50..5000]) : The maximum acceptable length of alignment.
   - *--max-fragment-length* or *-mfl* (**default value**: 2000, **valid range**: [50..5000]) : The maximum acceptable length of fragment.
   - *--max-num-locations* or *-mnl* (**default value**: 15, **valid range**: [1..100]) : The maximum number of location of candidate hits to examine.
   - *--alignment-band* or *-ab* (**default value**: 5, **valid range**: [1..100]) : The length of bound of banded alignment.
   - *--clipping-stringency* or *-ci* (**default value**: 1) : The stringency of soft-clipping.  
      0 : no soft clipping  
      1 : low  
      2 : medium  
      3 : high  
   - *--out-format* or *-of* : Writes the alignment files in BAM|SAM|NONE format. 
   - *--alignment-mode* or *-am* (**default value**: 2) : The alignment mode.  
      -1 : Competitive, none-bests    
      -2 : Competitive, random-best    
      -3 : Competitive, all-bests   
      1 : Normal, none-bests  
      2 : Normal, random-best  
      3 : Normal, all-bests  
      0 : Normal, all-hits  

   - *--interleaved* or *-i* : Process the fastq file as an interleaved paired-end archive.

- **version** or *v* : To show the versions of PanTools, JVM and Neo4j.
   
- **help** or **h**: To show the mannual of the tool.


## Visualization in the Neo4j browser
The Neo4j browser allows you to visualize parts of the pangenome graph and run Cypher queries and receive the results in a tabular or a graphical format. You need to download the appropriate version of Neo4j (use the version subcommand to see the consistent version). To visualize a pangenome: 

1.   Add path to the Neo4j /bin directory to the path environment variable.
2.   Add path to your pangenome in the Neo4j configuration file NEO4J-DIRECTORY/conf/neo4j.conf:

     ```
     dbms.directories.data = PATH_TO_THE_PANGENOME_DATABASE
     ```

3.   Start the Neo4j database server from the shell: `neo4j start`
4.   Open an internet browser and open http://localhost:7474.
5.   To visualize the whole pangenome, type this simple Cypher command in the browser: `MATCH (n) RETURN n`  
6.   Stop the Neo4j database server from the shell: `neo4j stop`  

