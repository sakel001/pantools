/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sequence;

import index.IndexScanner;
import index.kmer;
import static pantools.Pantools.DEBUG;
import static pantools.Pantools.complement;
import static pantools.Pantools.sym;

/**
 * Implements the class for scanning a 4-bit compressed sequence database. 
 * 
 * @author Siavash Sheikhizadeh, Bioinformatics chairgroup, Wageningen
 * University, the Netherlands
 */
public class SequenceScanner {
    private int genome;
    private int sequence;
    private int position;
    private int K;
    private kmer curr_kmer;
    private long curr_index;
    SequenceDatabase database;
    
    /**
     * Initializes a scanner object.
     * 
     * @param db The genome database
     * @param g The genome to scan
     * @param s The sequence to scan
     * @param k Size of Kmers
     * @param pre_len Prefix length of the Kmers
     */
    public SequenceScanner(SequenceDatabase db, int g, int s, int k, int pre_len){
        genome = g;
        sequence = s;
        database = db;
        position = 0;
        K = k;
        curr_kmer = new kmer(K, pre_len);
    }
    
    /**
     * @return The current scanned genome
     */
    public int get_genome(){
        return genome;
    }
    
    /**
     * @return The current scanned sequence
     */
    public int get_sequence(){
        return sequence;
    }
    
    /**
     * @return The current scanned position
     */
    public int get_position(){
        return position;
    }
    
    /**
     * @return The title of current scanned sequence 
     */
    public String get_title(){
        return database.sequence_titles[genome][sequence];
    }    
    
    /**
     * @return The offset of the current scanned sequence in the database
     */
    public long get_offset(){
        return database.sequence_offset[genome][sequence];
    } 
    
    /**
     * @return The current scanned kmer
     */
    public kmer get_curr_kmer(){
        return curr_kmer;
    }
    
    /**
     * @return The number of the current scanned kmer in the index database
     */
    public long get_curr_index(){
     return curr_index;   
    }   
    
    /**
     * @return The current scanned address 
     */
    public int[] get_address(){
        return new int[]{genome, sequence, position};
    }
    
    /**
     * @return The length of current scanned sequence
     */
    public long get_sequence_length(){
        return database.sequence_length[genome][sequence];
    }
    
    /**
     * Gives the length of a sequence in the database.
     * @param g The query genome
     * @param s The query sequence
     * @return The length of the query sequence
     */
    public long get_sequence_length(int g, int s){
        return database.sequence_length[g][s];
    }

    /**
     * Returns the binary code of the current scanned address.
     * 
     * @param offset The offset with respect to the current address
     * @return The binary code
     */
    public int get_code(int offset) {
        if (position + offset < database.sequence_length[genome][sequence] && position + offset > -1) {
            byte b;
            long pos = database.sequence_start[genome][sequence] + (position + offset) / 2;
            b = database.genomes_buff[(int) (pos / database.MAX_BYTE_COUNT)].get((int) (pos % database.MAX_BYTE_COUNT));
            if ((position + offset) % 2 == 0) {
                return (b >> 4) & 0x0f;
            } else {
                return (b & 0x0f);
            }
        } else {
            System.out.println("Wrong genomic position: " + (position + offset));
            return -1;
        }
    }

    /**
     * Returns the complement binary code of the current scanned address.
     * 
     * @param offset The offset with respect to the current address
     * @return The complement binary code
     */
    public int get_complement_code(int offset) {
        if (position + offset < database.sequence_length[genome][sequence]) {
            byte b;
            long pos = database.sequence_start[genome][sequence] + (position + offset) / 2;
            b = database.genomes_buff[(int) (pos / database.MAX_BYTE_COUNT)].get((int) (pos % database.MAX_BYTE_COUNT));
            if ((position + offset) % 2 == 0) {
                return complement[(b >> 4) & 0x0f];
            } else {
                return complement[(b & 0x0f)];
            }
        } else {
            return -1;
        }
    }

    /**
     * Sets the current scanned genome.
     * @param g The genome to be set.
     */
    public void set_genome(int g){
        genome = g;
    }

    /**
     * Sets the current scanned sequence.
     * @param s The sequence to be set.
     */
    public void set_sequence(int s){
        sequence = s;
        position = 0;
    }

    /**
     * Sets the current scanned position.
     * @param p The position to be set.
     */
    public void set_position(int p){
        position = p;
    } 
    
    /**
     * Sets the number of current kmer in the index database
     * @param ci 
     */
    public void set_curr_index(long ci){
     curr_index = ci;   
    }

    /**
     * Determines if end of scan is reached
     * @return True if end of scan is reached, or False
     */
    public boolean end_of_scan(){
        return genome > database.num_genomes;
    }      
    
    /**
     * Determines if end of current genome is reached
     * @return True if end of current genome is reached, or False
     */
    public boolean end_of_genome(){
        return sequence > database.num_sequences[genome];
    }    
    
    /**
     * Increments the current genome 
     */
    public void next_genome(){
        ++genome;
        sequence = 1;
    }
    
    /**
     * Increments the current sequence
     */
    public void next_sequence(){
        ++sequence;
        position = 0;
    }
    
    /**
     * Increments the current position
     */
    public void next_position(){
        ++position;
    }
    
    /**
     * decrements the current position
     */
    public void previous_position(){
        --position;
    }

    /**
    * Reads a kmer from the current sequence starting at "start".
    * @param start The start position of the kmer
    * @retutn The position of the first base after the kmer or
    * MININT if the is no kmer at that position or
    * The negative of the current position if it passess a degenerate region.
    **/
    public int initialize_kmer(int start) {
        if (DEBUG) System.out.println("initialize_kmer at " + start);
        int i;
        curr_kmer.reset();
        position = start - 1;
        for (i = 0; i < K && position < get_sequence_length() - 1; ++i) {
            next_position();
            if (get_code(0) > 3) {
                if (DEBUG) System.out.println("jump_forward");
                if (jump_forward())
                    return - (position - K + 1); // success but passed a degenerate region
                else
                    return Integer.MIN_VALUE; // failed
            } else
                curr_kmer.next_fwd_kmer(get_code(0));
        }
        if (DEBUG) System.out.println(curr_kmer.toString());
        if (i == K)
            return position - K + 1; // success
        else 
            return Integer.MIN_VALUE; // failed
    }

    /**
     * Calculates the kmer after the current kmer. 
     * @return the kmer after the current kmer
     */
    public int next_kmer() {
        if (position < get_sequence_length() - 1){
            next_position();
            if (get_code(0) > 3){
                if (jump_forward())
                    return - (position - K + 1); // success but passed a degenerate region
                else
                    return Integer.MIN_VALUE; // failed
            } else {
                curr_kmer.next_fwd_kmer(get_code(0));
                return position - K + 1;
            }
        } else
            return Integer.MIN_VALUE; // failed
    }

    /**
     * Jump over an ambiguous region; at first, position points to the first position which degenerate starts, 
     * after jumping it points to the last base of the first K-mer after the ambiguous region. 
     */
    public boolean jump_forward() {
        int j;
        int base_code = get_code(0);
        do {
            curr_kmer.reset();
            while (base_code > 3 && position < get_sequence_length() - 1) {
                next_position();
                base_code = get_code(0);
            }
            curr_kmer.next_fwd_kmer(base_code);
            for (j = 0; j < K - 1 && position < get_sequence_length() - 1; ++j) {
                next_position();
                base_code = get_code(0);
                if (base_code > 3) {
                    break;
                }
                curr_kmer.next_fwd_kmer(base_code);
            }
        } while (base_code > 3 && position < get_sequence_length() - 1);
        if (DEBUG) System.out.println(curr_kmer.toString());
        return j == K - 1; // got valid kmer
    }

    /**
     * Gives the IUPAC symbol at a specified genomic position.
     * 
     * @param g The query genome
     * @param s The query sequence
     * @param p The query position
     * @return The symbol 
     */
    public char get_symbol(int g, int s, int p) {
        if (p < database.sequence_length[g][s]) {
            byte b;
            long pos = database.sequence_start[g][s] + p / 2;
            b = database.genomes_buff[(int) (pos / database.MAX_BYTE_COUNT)].get((int) (pos % database.MAX_BYTE_COUNT));
            if (p % 2 == 0) {
                return sym[(b >> 4) & 0x0f];
            } else {
                return sym[b & 0x0f];
            }
        } else {
            return 0;
        }
    }

    /**
     * Gives complement of the IUPAC symbol at a specified genomic position.
     * 
     * @param g The query genome
     * @param s The query sequence
     * @param p The query position
     * @return The complement symbol 
     */
    public char get_complement_symbol(int g, int s, int p) {
        if (p < database.sequence_length[g][s]) {
            byte b;
            long pos = database.sequence_start[g][s] + p / 2;
            b = database.genomes_buff[(int) (pos / database.MAX_BYTE_COUNT)].get((int) (pos % database.MAX_BYTE_COUNT));
            if (p % 2 == 0) {
                return sym[complement[(b >> 4) & 0x0f]];
            } else {
                return sym[complement[(b & 0x0f)]];
            }
        } else {
            return 0;
        }
    }

    /**
     * Gives the binary code at a specified genomic position.
     * 
     * @param g The query genome
     * @param s The query sequence
     * @param p The query position
     * @return The binary code 
     */
    public int get_code(int g, int s, int p) {
        if (p < database.sequence_length[g][s] && p > -1) {
            byte b;
            long pos = database.sequence_start[g][s] + p / 2;
            b = database.genomes_buff[(int) (pos / database.MAX_BYTE_COUNT)].get((int) (pos % database.MAX_BYTE_COUNT));
            if (p % 2 == 0) {
                return (b >> 4) & 0x0f;
            } else {
                return (b & 0x0f);
            }
        } else {
            System.out.println(p + " is not in range 0.." + database.sequence_length[g][s]);
            return -1;
        }
    }

    /**
     * Gives complement of the binary code at a specified genomic position.
     * 
     * @param g The query genome
     * @param s The query sequence
     * @param p The query position
     * @return The complement code 
     */
    public int get_complement_code(int g, int s, int p) {
        if (p < database.sequence_length[g][s]) {
            byte b;
            long pos = database.sequence_start[g][s] + p / 2;
            b = database.genomes_buff[(int) (pos / database.MAX_BYTE_COUNT)].get((int) (pos % database.MAX_BYTE_COUNT));
            if (p % 2 == 0) {
                return complement[(b >> 4) & 0x0f];
            } else {
                return complement[(b & 0x0f)];
            }
        } else {
            return -1;
        }
    }
    
    /**
     * Retrieves a genomic region from the genome database.
     * 
     * @param g The query genome
     * @param s The query sequence
     * @param p The query position
     * @param l Length of the region
     * @param direction specifies the direction, True for forward and False for reverse
     * @return The genomic region
     */
    public void get_sub_sequence(StringBuilder seq, int g, int s, int p, int l, boolean direction) {
        int i;
        if (p >= 0 && p + l <= database.sequence_length[g][s]){
            if (direction) {
                for (i = 0; i < l; ++i) {
                    seq.append(get_symbol(g, s, p + i));
                }
            } else {
                for (i = l - 1; i >= 0; --i) {
                    seq.append(get_complement_symbol(g, s, p + i));
                }
            }
        } else
            System.err.println("Reading out of range!");
    } 

    /**
     * Retrieves the title of a sequence.
     * 
     * @param g The query genome
     * @param s The query sequence
     * @return The title
     */
    public void get_sequence_title(StringBuilder title, int g, int s) {
        title.append(database.sequence_titles[g][s]);
    } 
    
    /**
     * Determines the identity of two genomic regions.
     * 
     * @param a1 The address of the first region
     * @param a2 The address of the secing region
     * @param offset1 The offset from the first position
     * @param offset2 The offset from the second position
     * @param len The length of the regions
     * @param direction The direction of the comparison
     * @return True if regions are identical, or False
     */
    public boolean compare(int[] a1, int[] a2, int offset1, int offset2, int len, boolean direction) {
        if (a1[2] + offset1 + len - 1 >= database.sequence_length[a1[0]][a1[1]] || 
                a2[2] + offset2 + len - 1 >= database.sequence_length[a2[0]][a2[1]]) {
            return false;
        }
        int i;
        boolean equal;
        if (direction) {
            for (equal = true, i = 0; i < len && equal; ++i) {
                if (get_code(a1[0], a1[1], a1[2] + offset1 + i) != get_code(a2[0], a2[1], a2[2] + offset2 + i)) {
                    equal = false;
                }
            }
        } else {
            for (equal = true, i = 0; i < len && equal; ++i) {
                if (get_code(a1[0], a1[1], a1[2] + offset1 + i) != get_complement_code(a2[0], a2[1], a2[2] + offset2 + len - i - 1)) {
                    equal = false;
                }
            }
        }
        return equal;
    }  
    
    /**
     * Makes a kmer located at a genomic position.
     * 
     * @param genome The query genome
     * @param sequence The query sequence
     * @param position The query position
     * @return The kmer 
     */
    public kmer make_kmer(int genome, int sequence, int position) {
        int j,fwd_code;
        kmer tmp_kmer=new kmer(curr_kmer);
        tmp_kmer.reset();
        for(j = 0; j < K; ++j) {
            fwd_code=get_code(genome, sequence, position+j);
            tmp_kmer.next_fwd_kmer(fwd_code);
        }   
        return tmp_kmer;             
    }   
    
    /**
     * Finds the current scanning kmer in the kmer index.
     * 
     * @param inx The index scanner object
     * @return The number of kmer in the index
     */
    public long find_curr_kmer(IndexScanner inx){
        curr_index = inx.find(curr_kmer);
        return curr_index;
    }
        
}
