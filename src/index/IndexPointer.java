package index;

/**
 * Implements the data structure for index pointer. 
 * 
 * @author Siavash Sheikhizadeh, Bioinformatics chairgroup, Wageningen
 * University, the Netherlands
 */
public class IndexPointer {

    public long node_id;
    public int offset;
    public boolean canonical;
    public long next_index; // points to the next kmer in the index

    /**
     * The default constructor of the class
     */
    public IndexPointer() {
        node_id = -1L;
        canonical = false;
        offset = -1;
        next_index = -1L;
    }

    /**
     * The non-default constructor of the class
     * @param id The id of the node pointer points to
     * @param c Determines if the pointer points to the forward side 
     * @param p The zero-based position in the node 
     * @param n Number of the the next kmer in the index
     */
    public IndexPointer(long id, boolean c, int p, long n) {
        node_id = id;
        canonical = c;
        offset = p;
        next_index = n;
    }

    /**
     * Clears the content of the pointer.
     */
    public void reset() {
        node_id = -1L;
        canonical = false;
        offset = -1;
        next_index = -1l;
    }
    
    /**
     * @return The content of the pointer to be printed.
     */
    @Override
    public String toString(){
        return Long.toString(node_id) + " " + Boolean.toString(canonical) + " " + Integer.toString(offset);
    }

}
