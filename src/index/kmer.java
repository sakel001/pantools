package index;

import org.bouncycastle.util.Arrays;

/**
 * Implements the data structure for a kmer. 
 * 
 * @author Siavash Sheikhizadeh, Bioinformatics chairgroup, Wageningen
 * University, the Netherlands
 */
public class kmer {
    private int K; 
    private int prefix_length;   
    private int suffix_length;
    private int prefix_mask;   // A binary code with just prefix_length 1's on the right
    private int shift;         // Number of left shifts to reach to the position of left most base in the kmer 
    private int fwd_prefix;
    private byte[] fwd_suffix;
    private int rev_prefix;
    private byte[] rev_suffix;
    private boolean canonical;
    
    /**
     * Initializes an empty kmer object
     * 
     * @param k Size of K
     * @param p_len The length of the prefix of the kmer
     */
    public kmer(int k, int p_len)
    {
        K = k;
        prefix_length = p_len;
        suffix_length = K - p_len;
        prefix_mask = (1 << (2*prefix_length)) - 1;
        shift = 2 * (prefix_length - 1);
        fwd_suffix=new byte[suffix_length / 4];
        rev_suffix=new byte[suffix_length / 4];
        fwd_prefix=0;
        for(int i = 0;i < fwd_suffix.length; ++i)
            fwd_suffix[i] = 0;
        rev_prefix = 0;
        for(int i = 0; i < rev_suffix.length; ++i)
            rev_suffix[i] = 0;
        canonical=false;
    }

    /**
     * Copies a given kmer.
     * 
     * @param k_mer The kmer object to be copied. 
     */
    public kmer(kmer k_mer)
    {
        K = k_mer.K;
        prefix_length = k_mer.prefix_length;
        suffix_length = k_mer.suffix_length;
        prefix_mask = k_mer.prefix_mask;
        shift = k_mer.shift;
        fwd_prefix = k_mer.fwd_prefix;
        fwd_suffix = Arrays.clone(k_mer.fwd_suffix);
        rev_prefix = k_mer.rev_prefix;
        rev_suffix = Arrays.clone(k_mer.rev_suffix);
        canonical = k_mer.canonical;
    }
    
    /**
     * @return The prefix of the canonical form of the kmer
     */
    public int get_canonical_prefix(){
        return canonical ? fwd_prefix : rev_prefix;
    }
    
    /**
     * @return The suffix of the canonical form of the kmer
     */
    public byte[] get_canonical_suffix(){
        return canonical ? fwd_suffix : rev_suffix;
    }

    /**
     * @return True if the k-mer is canonical, or False
     */
    public boolean get_canonical(){
        return canonical;
    }

    /**
     * Sets the prefix of the forward kmer
     * 
     * @param p The prefix to set  
     */
    public void set_fwd_prefix(int p){
        fwd_prefix = p;
    }    
    
   /**
     * Sets the prefix of the reverse kmer
     * 
     * @param p The prefix to set  
     */
    public void set_rev_prefix(int p){
        rev_prefix = p;
    }    
    
    /**
     * Sets the suffix of the forward kmer
     * 
     * @param p The suffix to set  
     */
    public void set_fwd_suffix(byte[] s){
        fwd_suffix = s;
    }    

    /**
     * Sets the suffix of the reverse kmer
     * 
     * @param p The suffix to set  
     */
    public void set_rev_suffix(byte[] s){
        rev_suffix = s;
    }    

    /**
     * Sets the canonical value
     * 
     * @param c The new value for canonical field  
     */
    public void set_canonical(boolean c){
        canonical = c;
    } 

    /**
     * @return the forward prefix
     */
    public int get_fwd_prefix(){
        return fwd_prefix;
    }    
    
    /**
     * @return the forward suffix
     */
    public byte[] get_fwd_suffix(){
        return fwd_suffix;
    }    
    
    /**
     * Copies a kmer considering the possible difference in prefix lengths 
     * 
     * @param kmer1 The kmer to copy from
     * @param kmer2 The kmer to copy to 
     */
    public static void copy_and_adjust(kmer kmer2, kmer kmer1){
        int i, j, k, suffix_length1, suffix_length2;
        suffix_length1 = kmer1.suffix_length;
        suffix_length2 = kmer2.suffix_length;
        if (suffix_length1 != suffix_length2){
            for(i = suffix_length1/4 - 1, j = suffix_length2/4 - 1; i >= 0 && j >= 0; --i, --j)
                kmer2.fwd_suffix[j] = kmer1.fwd_suffix[i];  
            if (i >= 0){
                for (kmer2.fwd_prefix = kmer1.fwd_prefix, k = 0; k <= i; ++k)
                    kmer2.fwd_prefix = (kmer2.fwd_prefix << 8) | (kmer1.fwd_suffix[k] & 0x00FF);
            }
            if (j >= 0){
                kmer2.fwd_prefix = kmer1.fwd_prefix;
                for (;j >= 0; --j){
                    kmer2.fwd_suffix[j] = (byte)(kmer2.fwd_prefix & 0x0FF);
                    kmer2.fwd_prefix = kmer2.fwd_prefix >> 8;
                }
            }
        } else{
            kmer2.set_fwd_suffix(Arrays.clone(kmer1.get_fwd_suffix()));
            kmer2.set_fwd_prefix(kmer1.get_fwd_prefix());
        }
        kmer2.canonical = true; 
    }
    
    /**
     * Clears the content of the kmer
     */
    public void reset()
    {
        fwd_prefix=0;
        for(int i = 0;i < fwd_suffix.length; ++i)
            fwd_suffix[i] = 0;
        rev_prefix = 0;
        for(int i = 0; i < rev_suffix.length; ++i)
            rev_suffix[i] = 0;
        canonical=false;
    }
    
    /**
     * Compares suffices of two kmers
     * 
     * @param suf1 The first suffix
     * @param suf2 The second suffix
     * @return -1 if the first suffix is smaller, 0 if they are equal and 1 if the second suffix is smaller
     */
    public static int compare_suffix(byte[] suf1, byte[] suf2){
        int i;
        for(i = 0; i < suf1.length; ++i)
            if(suf1[i] != suf2[i])
                break;
        if(i == suf1.length)
            return 0;
        else if((suf1[i] & 0x0ff) < (suf2[i] & 0x0ff) )
            return -1;
        else
            return 1;        
    }
    
    /**
     * Turns forward kmer into the next kmer
     * @param base_code The binary code of the base to comes at right end of the kmer
     */
    public void next_fwd_kmer(int base_code)
    {
        int i;
        fwd_prefix=((fwd_prefix<<2) & prefix_mask) | ((fwd_suffix[0]>>6) & 0x03 );
        for(i = 0; i < fwd_suffix.length -1;++i)
           fwd_suffix[i]=(byte)((fwd_suffix[i]<<2) | (( fwd_suffix[i+1]>>6) & 0x03)); 
        fwd_suffix[i]=(byte)((fwd_suffix[i]<<2) | base_code); 
        base_code = 3 - base_code;
        for(i = rev_suffix.length-1; i > 0; --i)
            rev_suffix[i]=(byte)(((rev_suffix[i]>>2) & 0x03f) | ((rev_suffix[i-1] & 0x03 )<<6)); 
        rev_suffix[i]=(byte)(((rev_suffix[i]>>2) & 0x03f) | ((rev_prefix & 0x03 )<<6)); 
        rev_prefix=(rev_prefix>>2) | (base_code<<shift); 

        if(fwd_prefix < rev_prefix)
            canonical = true;
        else if(fwd_prefix > rev_prefix)
            canonical = false;
        else
            canonical = compare_suffix(fwd_suffix, rev_suffix) <= 0;
    }

    /**
     * Turns the forward kmer into the previous kmer
     * @param base_code The binary code of the base comes at left end of the kmer
     */
    public void prev_fwd_kmer(int base_code)
    {
        int i;
        for(i=fwd_suffix.length-1;i>0;--i){
            fwd_suffix[i]=(byte)(((fwd_suffix[i]>>2) & 0x03f) | ((fwd_suffix[i-1] & 0x03 )<<6)); 
        }
        fwd_suffix[i]=(byte)(((fwd_suffix[i]>>2) & 0x03f) | ((fwd_prefix & 0x03 )<<6)); 
        fwd_prefix=(fwd_prefix>>2) | (base_code<<shift); 
        base_code = 3 - base_code;
        rev_prefix=((rev_prefix<<2) & prefix_mask) | ((rev_suffix[0]>>6) & 0x03 );
        for(i=0;i<rev_suffix.length-1;++i)
           rev_suffix[i]=(byte)((rev_suffix[i]<<2) | (( rev_suffix[i+1]>>6) & 0x03)); 
        rev_suffix[i]=(byte)((rev_suffix[i]<<2) | base_code); 

        if(fwd_prefix < rev_prefix)
            canonical = true;
        else if(fwd_prefix > rev_prefix)
            canonical = false;
        else
            canonical = compare_suffix(fwd_suffix, rev_suffix) <= 0;
    }    

    /**
     * @return The string representing the kmer to be printed
     */
    @Override
    public String toString()
    {
        char[] sym=new char[]{ 'A', 'C', 'G' , 'T', 'M','R','W','S','Y','K','V','H','D','B','N'};
        StringBuilder fwd_seq=new StringBuilder(K);
        StringBuilder rev_seq=new StringBuilder(K);
        build_sequence(sym, fwd_prefix, prefix_length,fwd_seq);
        for(int i=0; i < fwd_suffix.length; ++i)
            build_sequence(sym, fwd_suffix[i], 4, fwd_seq);
        build_sequence(sym, rev_prefix, prefix_length,rev_seq);
        for(int i=0; i < rev_suffix.length; ++i)
            build_sequence(sym, rev_suffix[i], 4, rev_seq);
        return fwd_seq.append(" ").append(rev_seq).toString();
    }  

    /**
     * Recursively decodes the binary kmer into a string  
     * @param sym Array of symbols
     * @param word The binary string to be decoded
     * @param k Size of K
     * @param seq The string builder for the decoded kmer
     */
    private void build_sequence(char[] sym, int word,int k, StringBuilder seq )
    {
        if(k > 0)
        {
            build_sequence(sym, word >> 2,k-1,seq);
            seq.append(sym[word & 0x00000003]);
        }
    } 
}